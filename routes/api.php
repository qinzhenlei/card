<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/test', 'CardController@test');
Route::post('/admin/login', 'CardController@adminLogin');
Route::post('/user/receive', 'CardController@userReceive');
Route::get('/admin/orders', 'CardController@getOrders');
Route::post('/admin/packagesdetail', 'CardController@getPackages');
Route::get('/admin/packages', 'CardController@getPackagesList');
Route::post('/admin/createpackages', 'CardController@createPackages');
Route::post('/admin/updatepackage', 'CardController@updatePackages');
Route::post('/admin/orderdetail', 'CardController@getOrder');
Route::post('/admin/updateorder', 'CardController@updateOrder');
Route::post('/admin/confirmorder', 'CardController@confirmOrder');
Route::post('/admin/removeorder', 'CardController@removeOrder');
Route::post('/admin/createcode', 'CardController@createCode');
Route::post('/admin/codesdetail', 'CardController@getCodes');
Route::get('/admin/codes', 'CardController@getCodesList');
Route::post('/admin/delete', 'CardController@delete');
