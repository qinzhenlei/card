<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Middleware\RequestLog;


class CardController extends Controller
{
	private $courier = '顺丰';
    public $_time = 0;

	public function __construct(Request $request) {
        $this->request = $request;
        $this->_time = time();
    }

    /**
     * 管理员登录
     * @Author   qinzhenlei
     * @DateTime 2019-11-02
     * @param    string    $request  用户名
     * @param    string    $request  密码
     */
    public function adminLogin()
    {
        $name = $this->request->input('name','');
        $pwd = $this->request->input('pwd','');

        if (!$name || !$pwd) {
            $res = ['status'=>1, 'msg'=>'缺少参数'];
            return response()->json($res);
        }

        $admininfo = DB::table("user")->where([["username", $name], ["password", md5($pwd)], ['type', 2], ['del', 0]])
                                      ->first();
        if (!$admininfo) {
            $res = ['status'=>1, 'msg'=>'此用户不存在'];
            return response()->json($res);
        }

        $admininfo = (array)$admininfo;
        $res = ['status'=>0, 'msg'=>'ok', 'adminid'=>$admininfo['id']];
        return response()->json($res);
    }

    /**
     * 用户领取礼包
     * @Author   qinzhenlei
     * @DateTime 2019-11-03
     */
    public function userReceive()
    {
        $params = ['username', 'mobile', 'city', 'address', 'code'];
        $params_value = ['username'=>'用户名', 'mobile'=>'手机号', 'city'=>'城市', 'address'=>'详细地址', 'code'=>'邀请码'];
        $userdata = [];
        $orderdata = [];
        foreach ($params as $value) {
            $tmpvalues = trim($this->request->input($value, ''));
            if (!$tmpvalues) {
                $res = ['status'=>1, 'msg'=>'缺少'.$params_value[$value].'参数'];
                return response()->json($res);
            }
            if ($value == 'city') {
                $citys = explode(' ', $tmpvalues);
                $orderdata['province'] = $userdata['province'] = isset($citys[0]) ? $citys[0] : '';
                $orderdata['city'] = $userdata['city'] = isset($citys[1]) ? $citys[1] : '';
                $orderdata['area'] = $userdata['area'] = isset($citys[2]) ? $citys[2] : '';
            }else {
                $orderdata[$value] = $userdata[$value] = $tmpvalues;
            }
        }

        //查询code 码是否存在
        $codeinfo = DB::table('codes')->where('code', $userdata['code'])
                                      ->first();
        if (!$codeinfo || ((array)$codeinfo)['status'] == 2) {
            $res = ['status'=>1, 'msg'=>'礼包码已经被领取或不存在'];
            return response()->json($res);
        }

        //查询用户
        // $userinfo = DB::table("user")->where([['type',1],['mobile',]])

        DB::beginTransaction();
        //录入用户
        unset($userdata['code']);
        $userres = $this->operaTable('user', array_merge($userdata, ['type'=>1]));
        if ($userres['status']) {
            return response()->json($userres);
        }
        $userid = $userres['id'];
        //录入订单
        $orderdata = array_merge($orderdata, [
                        'packages_id' => ((array)$codeinfo)['packages_id'],
                        'packages_name' => ((array)$codeinfo)['packages_name'],
                        'total_fee' => ((array)$codeinfo)['packages_price'],
                        'uid' => $userid,
                        'courier'=>$this->courier,
                        'tradeno'=>$this->getTradeno()
                    ]);
        $orderres = $this->operaTable('order', $orderdata);
        if ($orderres['status']) {
            return response()->json($userres);
        }
        $orderid = $orderres['id'];
        //跟新 code数据
        $bool = DB::table('codes')->where("id",((array)$codeinfo)['id'])
                                  ->update(['status'=>2,'uid'=>$userid,'receive_time'=>$this->_time]);
        if ($userid && $orderid && $bool) {
            DB::commit();
            $res = ['status'=>0, 'msg'=>'ok'];
            return response()->json($res);
        }else{
            DB::rollback();
            $res = ['status'=>1, 'msg'=>'领取失败，请稍候重试'];
            return response()->json($res);
        }
    }

    /**
     * 获取订单数据
     * @Author   qinzhenlei
     * @DateTime 2019-11-03
     */
    public function getOrders()
    {
        //获取 订单的数据
        $orders = DB::table("order")->where([['del',0],['show',1]])
                                    ->get()
                                    ->toArray();
        $returnorder = [];
        $statusnames = [0=>'stay', 1=>'already', 2=>'finish'];
        if ($orders) {
            foreach ($orders as $key => $value) {
                $value = (array)$value;
                if (isset($statusnames[$value['status']])) {
                    !isset($returnorder[$statusnames[$value['status']]]) && $returnorder[$statusnames[$value['status']]] = [];
                    $value['ctime'] = date('Y-m-d', $value['ctime']);
                    $value['total_fee'] = $this->operanum($value['total_fee']);
                    $value['city_format'] = $value['province'].' '. $value['city'].' '.$value['area'];
                    array_push($returnorder[$statusnames[$value['status']]], $value);
                }
            }
        }
        $res = ['status'=>0, 'msg'=>'ok', 'data'=>$returnorder];
        return response()->json($res);
        
    }

    /**
     * 获取订单详情
     * @Author   qinzhenlei
     * @DateTime 2019-11-03
     */
    public function getOrder()
    {
        $orderid = $this->request->input('orderid','');
        $orderinfo = $orderid ? DB::table("order")->where('id', $orderid)
                                       ->first() : [];
        $orderinfo = $orderinfo ? (array)$orderinfo : [];
        $orderinfo['ctime'] = date('Y-m-d', $orderinfo['ctime']);
        $orderinfo['city_format'] = $orderinfo['province'].' '. $orderinfo['city'].' '.$orderinfo['area'];
        if (!$orderinfo) {
            $msg = '订单不存在';
            $res = ['status'=>1, 'msg'=>$msg];
            return response()->json($res);
        }

        $res = ['status'=>0, 'msg'=>'ok', 'data'=>$orderinfo];
        return response()->json($res);

    }

    /**
     * 订单确认
     * @Author   qinzhenlei
     * @DateTime 2019-11-03
     */
    public function confirmOrder()
    {
        
        $orderid = $this->request->input('orderid','');
        $status = $this->request->input('status',0);
        $orderinfo = $orderid ? DB::table("order")->where('id', $orderid)
                                       ->first() : [];

        $orderinfo = $orderinfo ? (array)$orderinfo : [];
        if (!$orderid || !$orderinfo || !in_array($status, [1,2,4]) ) {
            $msg = '订单不存在';
            !in_array($status, [1,2,4]) && $msg = '订单状态参数不合法';
            $res = ['status'=>1, 'msg'=>$msg];
            return response()->json($res);
        }
        if (($orderinfo['status'] == 0 && in_array($status, [1,4])) || ($orderinfo['status'] == 1 &&  in_array($status, [2,4]))) {
            $updatedata = ['status'=>$status,'utime'=>$this->_time];
            $status == 1 && $updatedata['confirm_time'] = $this->_time;
            $status == 2 && $updatedata['complete_time'] = $this->_time;
            $res = $this->operaTable('order', $updatedata, $orderid, false);
            return response()->json($res);
        }else{
            $res = ['status'=>1, 'msg'=>'操作不合法，请正常操作'] ;
            return response()->json($res);
        }
        
    }

    /**
     * 修改订单
     */
    public function updateOrder()
    {
        $orderid = $this->request->input('orderid','');
        $packages_id = $this->request->input('packages_id','');
        $orderinfo = $orderid ? DB::table("order")->where('id', $orderid)
                                       ->first() : [];
        $packagesinfo = $packages_id ? DB::table("packages")->where('id', $packages_id)
                                       ->first() : [];

        $orderinfo = $orderinfo ? (array)$orderinfo : [];
        $packagesinfo = $packagesinfo ? (array)$packagesinfo : [];
        if (!$orderinfo || ($packages_id && !$packagesinfo) || ($orderinfo && $orderinfo['status'] == 2)) {
            !$orderinfo && $msg = '订单不存在';
            ($orderinfo && $orderinfo['status'] == 2) && $msg = '订单已完成不可操作';
            !$packagesinfo && $msg = '套餐不存在';
            $res = ['status'=>1, 'msg'=>$msg];
            return response()->json($res);
        }

        $params = ['packages_id','courier','express_no','username',',mobile','city','address','total_fee'];
        $updatedata = [];
        foreach ($params as $value) {
            $tmpvalues = trim($this->request->input($value, ''));
            if ($tmpvalues && $value == 'city') {
                $citys = explode(' ', $tmpvalues);
                $updatedata['province'] = isset($citys[0]) ? $citys[0] : '';
                $updatedata['city'] = isset($citys[1]) ? $citys[1] : '';
                $updatedata['area'] = isset($citys[2]) ? $citys[2] : '';
            }else {
                $tmpvalues && $updatedata[$value] = $tmpvalues;
                ($tmpvalues && $value == 'packages_id') && $updatedata['packages_name'] = $packagesinfo['name'];
            }
        }
        $orderinfo['total_fee'] = $this -> operanum($orderinfo['total_fee']);
        $updatedata = array_diff_assoc($updatedata, $orderinfo);
        if (empty($updatedata)) {
            $res = ['status'=>0, 'msg'=>'无更新'];
            return response()->json($res);
        }else{
            $res = $this->operaTable('order', $updatedata, $orderid, false);
            return response()->json($res);
        }

    }
    /**
     * 移除订单
     */
    public function removeOrder()
    {
        $orderid = $this->request->input('orderid','');
        if (!$orderid) {
            $res = ['status'=>1, 'msg'=>'缺少参数'];
            return response()->json($res);
        }
        $res = $this->operaTable('order', ['show'=>0], $orderid);
        return response()->json($res);
    }

    /**
     * 获取 套餐列表
     */
    public function getPackagesList() {
        //获取 订单的数据
        $packageslist = DB::table("packages")->where('del',0)
                                    ->get()
                                    ->toArray();
        foreach ($packageslist as &$value) {
            $value = (array)$value;
            $value['utime'] = date("Y-m-d", $value['utime']);
            $value['price'] = $this->operanum($value['price']);
        }
        $res = ['status'=>0, 'msg'=>'ok', 'data'=>$packageslist];
        return response()->json($res);
    }

    /**
     * 获取 套餐详情
     */
    public function getPackages() {
        $id = $this->request->input('id',0);
        $packagesinfo = $id ? DB::table("packages")->where('id', $id)
                                                  ->first() : $id;
        if (!$id || !$packagesinfo) {
            $res = ['status'=>1, 'msg'=>!$id ? '缺少ID' : '套餐不存在'];
            return response()->json($res);
        }
        $packagesinfo['price'] = $this->operanum($packagesinfo['price']);
        $res = ['status'=>0, 'msg'=>'ok', 'data'=>$packagesinfo];
        return response()->json($res);
        
    }

    /**
     * 创建套餐
     */
    public function createPackages()
    {
        $name = trim($this->request->input('name',''));
        $price = $this->request->input('price',0);

        if (!$name) {
            $res = ['status'=>1, 'msg'=>'套餐名称不能为空'];
            return response()->json($res);
        }
        $res = $this->operaTable('packages', ['name'=>$name, 'price'=>$price]);
        return response()->json($res);
        
    }

    /**
     * 修改套餐
     */
    public function updatePackages()
    {
        $id = trim($this->request->input('id',0));
        $name = trim($this->request->input('name',''));
        $price = $this->request->input('price',0);

        $packagesinfo = $id ? (DB::table('packages')->where('id', $id)->first()) : [];
        $packagesinfo && $packagesinfo = (array)$packagesinfo;
        if (!$packagesinfo) {
            $res = ['status'=>1, 'msg'=>'套餐不存在'];
            return response()->json($res);
        }
        $updatedata = ['name'=>$name, 'price'=>$price];
        $packagesinfo['price'] = $this -> operanum($packagesinfo['price']);
        $updatedata = array_diff_assoc($updatedata, $packagesinfo);
        if (empty($updatedata)) {
            $res = ['status'=>0, 'msg'=>'无更新'];
            return response()->json($res);
        }else{
            $res = $this->operaTable('packages', $updatedata, $id, false);
            return response()->json($res);
        }
        
    }

    /**
     * 创建 code码
     */
    public function createCode()
    {
        $num = $this->request->input('num',0);
        $packages_id = $this->request->input('packages_id',0);
        $packagesinfo = $packages_id ? (DB::table("packages")->where('id', $packages_id)
                                       ->first()) : [];
        $packagesinfo && $packagesinfo = (array)$packagesinfo;
        if (!$num || !$packages_id || !$packagesinfo) {
            $msg = '缺少参数';
            !$packagesinfo && $msg = '套餐不存在';
            $res = ['status'=>1, 'msg'=> $msg];
            return response()->json($res);
        }

        $comdata = ['packages_id'=>$packages_id, 'packages_name'=>$packagesinfo['name'], 'packages_price'=>$packagesinfo['price']];
        $success = [];
        $codelist = [];
        for ($i=0; $i < $num; $i++) {
            $tmprand = $this->randomkeys(16);
            $tmpcodeinfo = DB::table('codes')->where('code', $tmprand)->first();
            if (!$tmpcodeinfo) {
                $coderes = $this->operaTable('codes', array_merge($comdata, ['code'=>$tmprand, 'status'=>1]));
                if (!$coderes['status']) {
                    array_push($success, $coderes['id']);
                    array_push($codelist, $tmprand);
                }
            }
            
        }
        $res = ['status'=>0, 'msg'=>'ok', 'successnum'=>count($success), 'sucids'=>implode(',',$success),'codelist'=>$codelist];
        return response()->json($res);
    }

    /**
     * 获取 code码详情
     */
    public function getCodes() {
        $id = $this->request->input('id',0);
        $codesinfo = $id ? DB::table("codes")->where('id', $id)
                                                  ->first() : $id;
        $codesinfo && $codesinfo = (array)$codesinfo;
        if (!$id || !$codesinfo) {
            $res = ['status'=>1, 'msg'=>!$id ? '缺少ID' : '套餐不存在'];
            return response()->json($res);
        }
        $codesinfo['packages_price'] = $this->operanum($codesinfo['packages_price']);
        $codesinfo['utime'] = date('Y-m-d', $codesinfo['utime']);
        $res = ['status'=>0, 'msg'=>'ok', 'data'=>$codesinfo];
        return response()->json($res);
        
    }

    /**
     * 获取 codes列表
     */
    public function getCodesList() {
        //获取 订单的数据
        $codeslist = DB::table("codes")->where('del',0)
                                    ->get()
                                    ->toArray();
        foreach ($codeslist as &$value) {
            $value = (array)$value;
            $value['utime'] = date("Y-m-d", $value['utime']);
            $value['packages_price'] = $this->operanum($value['packages_price']);
        }
        $res = ['status'=>0, 'msg'=>'ok', 'data'=>$codeslist];
        return response()->json($res);
    }




    /**
     * 删除
     */
    public function delete() {
        $table = trim($this->request->input('table',''));
        $id = $this->request->input('id',0);

        if (!$table || !$id) {
            $res = ['status'=>1, 'msg'=>'缺少参数'];
            return response()->json($res);
        }
        $res = $this->operaTable($table, ['del'=>1], $id);
        return response()->json($res);
    }


    public function operaAdmin($data, $id=0)
    {
        if (!$id) { //入库
            $adminid = DB::table("user")->insertGetId($data);
        }else{
            $admininfo = DB::table("user")->where([['id']]);

        }
    }

    

    public function getTradeno()
    {
        $randnum = mt_rand(100,999);
        return date('Y').$this->_time.$randnum;
    }

   public function randomkeys($length){
       $key = '';
       $pattern = '1234567890ABCDEFGHIJKLOMNOPQRSTUVWXYZ';
       for($i=0;$i<$length;$i++)
       {
           $key .= $pattern{mt_rand(0,35)};  //生成php随机数
        }
        return $key;
    }

    public function operaTable($table , $data , $id=0, $issel = true)
    {
        if (!$id) { //入库
            $data = array_merge($data, ['ctime'=>$this->_time, 'utime'=>$this->_time]);
            $id = DB::table($table)->insertGetId($data);
        }else{
            if ($issel) {
                $tableinfo = DB::table($table)->where('id', $id)
                                        ->first();
                if (!$tableinfo) {
                    return ['status'=>1, 'msg'=>'数据不存在'];
                }
            }
            $bool = DB::table($table)->where('id', $id)->update($data);
            if (!is_int($bool)) {
                return ['status'=>1, 'msg'=>'操作失败，请稍候重试'];
            }
        }
        return ['status'=>0, 'msg'=>'ok', 'id'=>$id];
    }

    /**
     * 去除 将小数点后面 末尾0
     * @Author   qinzhenlei
     * @DateTime 2019-05-21
     * @param    float     $num  金额数据
     * @return  
     */
    public function operanum($num) {
        if (intval($num) == $num) {
            return intval($num);
        } elseif (intval($num * 10) == $num * 10) {
            return intval($num * 10) / 10;
        } else {
            return round($num, 2);
        }
    }


    public function test(Request $request){
    	echo 1;die;
    	$str = AppRedis::getInstance()->get('qzl-ceshi');
    	if ($str) {
    		var_dump($str);die;
    	}
    	AppRedis::getInstance()->setex('qzl-ceshi', time() , 60);
    	var_dump(time());die;
		echo "<pre>";
		print_r($wx_userinfos);
		die;
    }
	
}
